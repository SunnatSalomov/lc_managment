package com.company.lc_manager.enums;

public enum Roles {
    ADMIN, TEACHER, STUDENT
}
