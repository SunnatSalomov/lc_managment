package com.company.lc_manager.config.init;
import com.company.lc_manager.entity.Address;
import com.company.lc_manager.entity.User;
import com.company.lc_manager.enums.Gender;
import com.company.lc_manager.enums.Roles;
import com.company.lc_manager.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;

@Configuration
@RequiredArgsConstructor
public class LoadDatabase {

    private final UserRepository userRepository;

    @Bean
    CommandLineRunner initDatabase() {
        return args -> {
            User admin = User.builder()
                    .firstName("Admin")
                    .lastName("Adminov")
                    .gender(Gender.MALE)
                    .phoneNumber("+998999805970")
                    .password(new BCryptPasswordEncoder().encode("123"))
                    .role(Roles.ADMIN)
                    .email("sunish2412gmail.com")
                    .birthDay(LocalDate.of(1999,12,24))
                    .address(Address.builder().city("Chorqi").country("UZB").description("Susambil").build())
                    .username("admin")
                    .statusTech(false)
                    .build();
            if (userRepository.findByPhoneNumber("+998999805970").isEmpty())
                userRepository.save(admin);
        };
    }
}
