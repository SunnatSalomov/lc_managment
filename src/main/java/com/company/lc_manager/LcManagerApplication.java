package com.company.lc_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LcManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LcManagerApplication.class, args);
    }

}
