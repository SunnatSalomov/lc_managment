package com.company.lc_manager.dto;

import com.company.lc_manager.entity.Group;
import com.company.lc_manager.entity.Student;
import jakarta.persistence.Column;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AttendanceDTO {

    private String studentName;
    private String groupName;
    private LocalDate date;
    private boolean check;
}
