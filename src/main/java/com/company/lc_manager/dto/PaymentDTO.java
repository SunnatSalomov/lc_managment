package com.company.lc_manager.dto;

import com.company.lc_manager.entity.Group;
import com.company.lc_manager.entity.Student;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Month;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentDTO {

    private double cost;
    private String studentName;
    private Integer groupId;
    private Month month;

}
