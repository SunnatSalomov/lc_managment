package com.company.lc_manager.dto;

import com.company.lc_manager.entity.Course;
import com.company.lc_manager.enums.DaysOfWeek;
import com.company.lc_manager.enums.Types;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GroupDTO {
    private Integer courseId;
    private String daysOfWeek;
    private String types;
}
