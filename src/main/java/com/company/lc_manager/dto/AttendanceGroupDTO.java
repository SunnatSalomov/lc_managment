package com.company.lc_manager.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class AttendanceGroupDTO {
    private List<String> studentNames;
    private int groupId;
    private LocalDate date;
    private boolean check;

}
