package com.company.lc_manager.repository;

import com.company.lc_manager.dto.GroupDTO;
import com.company.lc_manager.entity.Group;
import com.company.lc_manager.enums.DaysOfWeek;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group,Long> {
    Group findByCourse_Id(Long course_id);
    Group findByDaysOfWeek(DaysOfWeek daysOfWeek);
    Group findByTeacherId(Long id);
}
