package com.company.lc_manager.repository;

import com.company.lc_manager.entity.Attachment;
import com.company.lc_manager.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface AttachmentRepository extends JpaRepository<Attachment,Long> {
}
