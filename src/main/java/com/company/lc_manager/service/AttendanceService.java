package com.company.lc_manager.service;

import com.company.lc_manager.dto.AttendanceDTO;
import com.company.lc_manager.dto.AttendanceGroupDTO;
import org.springframework.stereotype.Service;

@Service
public interface AttendanceService {
    AttendanceDTO getAttendanceByStudentIdAndMonth(Integer studentId, String month);

    AttendanceGroupDTO getAttendanceByGroupIdAndMonth(Integer groupId, String month);
}
