package com.company.lc_manager.service;

import com.company.lc_manager.dto.CourseDTO;
import com.company.lc_manager.dto.GroupDTO;
import com.company.lc_manager.dto.GroupEditDTO;
import com.company.lc_manager.dto.TeacherDTO;
import com.company.lc_manager.entity.Course;
import com.company.lc_manager.entity.Group;
import com.company.lc_manager.entity.Teacher;
import com.company.lc_manager.enums.DaysOfWeek;
import com.company.lc_manager.enums.Types;
import com.company.lc_manager.exception.RestException;
import com.company.lc_manager.repository.GroupRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;
    private final CourseService courseService;
    private final TeacherServiceImpl teacherService;

    @Override
    public GroupDTO addGroup(GroupDTO groupDTO) {
        Group group = mapToGroup(groupDTO);
        groupRepository.save(group);
        return groupDTO;
    }

    @Override
    public List<GroupDTO> listGroup() {
        List<Group> allGroup = groupRepository.findAll();

        return allGroup.stream().map(this::mapToGroupDTO).toList();
    }

    public Group mapToGroup(GroupDTO groupDTO){
        return Group.builder()
                .course(courseService.getCourseById(groupDTO.getCourseId()))
                .daysOfWeek(DaysOfWeek.valueOf(groupDTO.getDaysOfWeek()))
                .types(Types.valueOf(groupDTO.getTypes()))
                .build();
    }

    private GroupDTO mapToGroupDTO(Group group) {

          return GroupDTO.builder()
                .types(group.getTypes().name())
                .courseId(Math.toIntExact(group.getCourse().getId()))
                .daysOfWeek(group.getDaysOfWeek().name())
                .build();
    }
    @Override
    public GroupDTO getById(Integer id) {
        Group group = groupRepository.findById(id.longValue()).orElseThrow(() -> RestException.restThrow("Not found"));
        return mapToGroupDTO(group);
    }

    @Override
    public GroupDTO getByCourseId(Integer courseId) {
        Group byCourseId = groupRepository.findByCourse_Id(courseId.longValue());

        return mapToGroupDTO(byCourseId);
    }

    @Override
    public GroupDTO getByDay(String day) {
        Group byDaysOfWeek = groupRepository.findByDaysOfWeek(DaysOfWeek.valueOf(day));

        return mapToGroupDTO(byDaysOfWeek);
    }

    @Override
    public GroupDTO getByTeacherId(Integer id) {
        Group byTeacherId = groupRepository.findByTeacherId(id.longValue());

        return mapToGroupDTO(byTeacherId) ;
    }


    @Override
    public GroupEditDTO editGroup(Integer id, GroupEditDTO groupEditDTO) {
        Group group = groupRepository.findById(id.longValue()).orElseThrow(() -> RestException.restThrow("not Found"));

        group.setTeacher(teacherService.mapToTeacher(teacherService.getTeacherById(groupEditDTO.getCourseId())));
        group.setCourse(courseService.getCourseById(groupEditDTO.getCourseId()));
        group.setTypes(Types.valueOf(groupEditDTO.getTypes()));
        group.setDaysOfWeek(DaysOfWeek.valueOf(groupEditDTO.getDaysOfWeek()));
        return groupEditDTO;

    }



    @Override
    public boolean deleteGroup(Integer id) {
        groupRepository.deleteById(id.longValue());
        return true;
    }
}
