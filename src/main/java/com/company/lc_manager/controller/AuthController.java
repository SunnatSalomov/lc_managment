package com.company.lc_manager.controller;

import com.company.lc_manager.dto.LoginDTO;
import com.company.lc_manager.dto.RegisterDTO;
import com.company.lc_manager.dto.TokenDTO;
import com.company.lc_manager.utils.AppConstants;
import jakarta.validation.Valid;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(AuthController.BASE_PATH)
public interface AuthController {
    String BASE_PATH = AppConstants.AUTH_CONTROLLER_BASE_PATH;
    @PostMapping(AppConstants.AUTH_CONTROLLER_LOGIN_PATH)
    HttpEntity<TokenDTO> login(@Valid @RequestBody LoginDTO loginDTO);

    @PostMapping(AppConstants.AUTH_CONTROLLER_REGISTER_PATH)
    HttpEntity<TokenDTO> register(@Valid @RequestBody RegisterDTO registerDTO);

    @PatchMapping(AppConstants.AUTH_CONTROLLER_REFRESH_TOKEN_PATH)
    HttpEntity<TokenDTO> refreshToken(String accessToken, String refreshToken);


}
