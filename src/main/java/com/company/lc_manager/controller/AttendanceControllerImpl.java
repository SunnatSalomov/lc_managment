package com.company.lc_manager.controller;

import com.company.lc_manager.dto.ApiResult;
import com.company.lc_manager.dto.AttendanceDTO;
import com.company.lc_manager.dto.AttendanceGroupDTO;
import com.company.lc_manager.service.AttendanceService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AttendanceControllerImpl implements AttendanceController {
    private final AttendanceService attendanceService;
    @Override
    public ApiResult<AttendanceDTO> getAttendanceByStudentIdAndMonth(Integer id, String month) {
        return ApiResult.successResponse(attendanceService.getAttendanceByStudentIdAndMonth(id,month));
    }

    @Override
    public ApiResult<AttendanceGroupDTO> getAttendanceByGroupIdAndMonth(Integer id, String month) {

        return ApiResult.successResponse(attendanceService.getAttendanceByGroupIdAndMonth(id,month));
    }
}
